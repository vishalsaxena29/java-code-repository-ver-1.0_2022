# The JAVA World Project Files

A simple approach for the people who finds hard to learn new things and not good with coding uptill now.

{Overview of Java and Features of Java (What makes Java different from all other language.) }

This Project shows the Java Problems, Explanation  related to the following Topics:-

1. Arrays.
2. strings.
3. Collection Framework.
4. Multi Threading. 
5. OOPS Concepts and its Implementation. (Inheritance , Encapsulation , Abstraction , Polymorphism)
6. WRAPPER CLASSES , ABSTRACT (Class,method/Block),Final (keyword,method,Variable).
7. IOStreams.
8. Exception Handling.
9. Constructors.
10. Overview and Explanation of Structure of JVM , JRE and JDK.   

Feel free to contact me on my below mentioned email for any queries and explanation in respect with the above mentioned topics.
Happy Coding.
And never Forget "JAVA IS AWSOME".


--
Vishal Saxena
saxena.vishal204@gmail.com

